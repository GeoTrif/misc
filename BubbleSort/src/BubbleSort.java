
/**
 * Metoda BubbleSort este cel mai simplu algoritm de sortare care functioneaza interschimband in mod repetat elementele adiacente daca nu sunt in ordine crescatoare. 
 * Time complexity in cazul cel mai defavorabil este atunci cand vectorul este in ordine descrescatoare.Cazul mediu este atunci cand vectorul are elementele in ordine aleatoare,dar nu descrescatoare si crescatoare.
 * Cazul cel mai favorabil este atunci cand vectorul nostru este deja sortat.
 * Complexitatea in cazul cel mai defavorabil si in cazul mediu este de O(n * n).In cazul cel mai favorabil este O(n).
 * Acest algoritm este folosit doar pentru introducerea in conceptul algoritmilor de sortare.

 */
import java.util.Arrays;
import java.util.Scanner;

public class BubbleSort {

	public static void main(String[] args) { // Metoda main = metoda Driver de testare a metodei BubbleSort.

		Scanner input = new Scanner(System.in);

		System.out.println("Input the number of elements of the array: ");
		int elementNum = input.nextInt();

		int[] myArray = new int[elementNum];

		for (int i = 0; i < myArray.length; i++) {

			System.out.println("Enter the value found at index " + i + " :");
			myArray[i] = input.nextInt();

		}

		System.out.println("The original array is: " + Arrays.toString(myArray));

		int[] sortedArray = bubbleSort(myArray);

		System.out.println("The sorted array is: " + Arrays.toString(sortedArray));

	}

	public static int[] bubbleSort(int[] array) { // Metoda care implementeaza Bubble Sort.

		int arrayLen = array.length;
		int temp = array[0];

		for (int i = 0; i < arrayLen - 1; i++) { // Bubble Sort utilizeaza metoda BruteForce cu 2 for-uri.
			for (int j = 0; j < arrayLen - i - 1; j++) {
				if (array[j] > array[j + 1]) {

					temp = array[j]; // Facem interschimbare de variabile utilizand o a treia variabila temp.
					array[j] = array[j + 1];
					array[j + 1] = temp;

				}
			}
		}

		return array;

	}

}
