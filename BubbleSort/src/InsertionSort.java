
/**
 * Metoda Insertion Sort este un algoritm simplu de sortare care functioneaza la fel cum sortam niste carti de joc in mana.
 * Time complexity in cazul cel mai defavorabil este atunci cand vectorul este in ordine descrescatoare.Cazul mediu este atunci cand vectorul are elementele in ordine aleatoare,dar nu descrescatoare si crescatoare.
 * Cazul cel mai favorabil este atunci cand vectorul nostru este deja sortat.
 * Complexitatea in cazul cel mai defavorabil si in cazul mediu este de O(n * n).In cazul cel mai favorabil este O(n).
 * Paradigma algoritmului este abordarea incrementarii.
 * Acest algoritm este folosit atunci cand avem un vector cu putine elemente.
 */

import java.util.Arrays;
import java.util.Scanner;

public class InsertionSort {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Enter the number of elements of the array: ");
		int elements = input.nextInt();

		int[] myArray = new int[elements];

		for (int i = 0; i < myArray.length; i++) {

			System.out.println("Enter the element at index " + i + " :");
			myArray[i] = input.nextInt();

		}

		System.out.println("The original array is: " + Arrays.toString(myArray));

		int[] sortedArray = sort(myArray);

		System.out.println("The sorted array is: " + Arrays.toString(sortedArray));

	}

	public static int[] sort(int[] array) { // Metoda pentru a sorta vectorul cu algoritmul insertion sort.

		int arrayLen = array.length;

		for (int i = 1; i < arrayLen; ++i) {

			int key = array[i];

			int j = i - 1;

			while (j >= 0 && array[j] > key) { // Mutarea elementelor din array[0..i-1],care sunt mai mari decat key,cu
												// o pozitie mai in fata decat pozitia curenta.

				array[j + 1] = array[j];
				j = j - 1;
			}

			array[j + 1] = key;

		}

		return array;
	}

}
