package com.sda.gitapp.model;

public class Animal {
	private String colour;
	private int weight;

	public void eat() {
		System.out.println("Animal eats");
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

}
