
import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class CSVExample {

	public static void main(String[] args) throws Exception {

		File f = new File("C:\\Users\\GeoTrif\\Desktop\\excel.xlss");

		Scanner input = new Scanner(System.in);

		System.out.println("Input the number of elements of the array: ");
		int elementNum = input.nextInt();

		int[] myArray = new int[elementNum];

		for (int i = 0; i < myArray.length; i++) {

			System.out.println("Enter the value found at index " + i + " :");
			myArray[i] = input.nextInt();

		}

		System.out.println("The original array is: " + Arrays.toString(myArray));

		int[] sortedArray = bubbleSort(myArray);

		System.out.println("The sorted array is: " + Arrays.toString(sortedArray));

		WritableWorkbook myExcel = Workbook.createWorkbook(f);
		WritableSheet mySheet = myExcel.createSheet("mySheet", 0);
		Label l1 = new Label(0, 0, " data 1");
		// Label l2 = new Label(0, 1, "data 2");
		mySheet.addCell(l1);
		// mySheet.addCell(l2);
		myExcel.write();
		myExcel.close();

		System.out.println("Excel created.");

	}

	public static int[] bubbleSort(int[] array) { // Metoda care implementeaza Bubble Sort.

		int arrayLen = array.length;
		int temp = array[0];

		for (int i = 0; i < arrayLen - 1; i++) { // Bubble Sort utilizeaza metoda BruteForce cu 2 for-uri.
			for (int j = 0; j < arrayLen - i - 1; j++) {
				if (array[j] > array[j + 1]) {

					temp = array[j]; // Facem interschimbare de variabile utilizand o a treia variabila temp.
					array[j] = array[j + 1];
					array[j + 1] = temp;

				}
			}
		}

		return array;

	}

}
