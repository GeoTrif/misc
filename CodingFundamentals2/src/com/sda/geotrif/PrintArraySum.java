package com.sda.geotrif;

/*
 * Create a program that prints the sum of all the elements from an array.
 * Ex:Enter a number: 1
Enter a number: 2
Enter a number: 3
Enter a number: 4
Enter a number: 5
The total is 15.

Also print the minimum and the maximum elements from the array.
 */

import java.util.Scanner;

public class PrintArraySum {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Enter the number of elements from the array: ");
		int arrayLen = input.nextInt();

		int[] myArray = new int[arrayLen];

		for (int i = 0; i < arrayLen; i++) {
			System.out.print("Enter the element at index " + i + " :");
			myArray[i] = input.nextInt();
		}

		System.out.println("The sum of the elements is: " + printArraySum(myArray));

		System.out.println("The minimum element is: " + findMin(myArray));
		System.out.println("The maximum element is: " + findMax(myArray));

	}

	public static int printArraySum(int[] array) {

		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		return sum;

	}

	public static int findMin(int[] array) {

		int min = Integer.MAX_VALUE;
		for (int i = 0; i < array.length; i++) {
			if (array[i] < min) {
				min = array[i];
			}
		}

		return min;

	}

	public static int findMax(int[] array) {

		int max = Integer.MIN_VALUE;
		for (int i = 0; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		return max;
	}

}
