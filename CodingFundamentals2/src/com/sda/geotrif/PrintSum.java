package com.sda.geotrif;

import java.util.Scanner;

public class PrintSum {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int number = 0;
		int sum = 0;

		do {
			System.out.print("Enter an integer: ");
			number = input.nextInt();
			sum += number;
		} while (number != 0);

		System.out.println("The sum of the numbers is: " + sum);

		System.out.println(" ------------------------------ ");

		printSum();
	}

	public static void printSum() {

		Scanner input = new Scanner(System.in);
		System.out.println("Enter the number of elements: ");
		int contor = input.nextInt();

		int sum = 0;
		int i = 0;
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;

		while (contor != i) {
			System.out.println("Enter an integer: ");
			int number = input.nextInt();
			sum += number;

			if (number < min) {
				min = number;
			}
			if (number > max) {
				max = number;
			}

			i++;
		}

		System.out.println("The sum of the elements is : " + sum);

		System.out.println("The minimum is: " + min);
		System.out.println("The maximum is: " + max);
	}

}
