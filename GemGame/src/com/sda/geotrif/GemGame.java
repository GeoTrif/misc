package com.sda.geotrif;

/*
 * A robot moves in a plane (10x10) starting from the original point (0,0) to get a gem from the map random placed on every run. The robot can move toward UP, DOWN, LEFT and RIGHT with a given steps. The trace of robot movement is shown as the following:
UP 5
DOWN 3
LEFT 3
RIGHT 2
The numbers after the direction are steps. Please write a program to print the board with robot and gem, then at each move update the board and print the new position of the robot. If an invalid move is done print an error message. When the gem is reached print a message with number of moves done until gem was reached and end the game.
Example:
X * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * o * * * 
* * * * * * * * * * 
* * * * * * * * * * 
DOWN 7
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
X * * * * * o * * * 
* * * * * * * * * * 
* * * * * * * * * * 
RIGHT 6
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * * * * * 
* * * * * * X * * * 
* * * * * * * * * * 
* * * * * * * * * * 
Congratulations! You finished the game in 2 moves.
 */

import java.util.Random;
import java.util.Scanner;

public class GemGame {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		Random random = new Random();
		int ranRow = random.nextInt(10);
		int ranCol = random.nextInt(10);

		System.out.println("BoardGame: Find the gem.");
		printInitialBoard(ranRow, ranCol);
		System.out.println("---------------------------");

		System.out.print("Enter the row and col: ");
		int row = input.nextInt();
		int col = input.nextInt();
		int movesContor = 1;

		while (row != ranRow || col != ranCol) {

			printModifiedBoard(row, col, ranRow, ranCol);
			++movesContor;
			System.out.print("Enter the row and col: ");
			row = input.nextInt();
			col = input.nextInt();
		}

		if (row == ranRow && col == ranCol) {
			printWinningMessage(movesContor, row, col, ranRow, ranCol);
		}
	}

	public static void printInitialBoard(int ranRow, int ranCol) {

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {

				if (i == 0 && j == 0) {
					System.out.print("X ");
				} else if (i == ranRow && j == ranCol) {
					System.out.print("o ");
				} else {
					System.out.print("* ");
				}
			}
			System.out.println();
		}
	}

	public static void printModifiedBoard(int row, int col, int ranRow, int ranCol) {

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (i == row && j == col) {
					System.out.print("X ");
				} else if (i == ranRow && j == ranCol) {
					System.out.print("o ");
				} else {
					System.out.print("* ");
				}

			}
			System.out.println();
		}
	}

	public static void printWinningMessage(int moves, int row, int col, int ranRow, int ranCol) {

		System.out.println("Congratulations!You finished the game in " + moves + " moves.");

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (i == row && j == col) {
					System.out.print("X ");
				} else if (i == ranRow && j == ranCol) {
					System.out.print("o ");
				} else {
					System.out.print("* ");
				}

			}
			System.out.println();
		}

	}
}
