package com.sda.geotrif;

import java.util.Random;
import java.util.Scanner;

/*
 * Write a Guess the Number game that has three levels of difficulty. The first level of difficulty would be a number between 1 and 10. The second difficulty
set would be between 1 and 100. The third difficulty set would be between 1 and 1000. 
Prompt for the difficulty level, and then start the game.The computer picks a random number in that range and prompts the player to guess that number. 
Each time the player guesses, the compute should give the player a hint as to whether the number is too high or too low. 
The computer should also keep track of the number of guesses. Once the player guesses the correct number, the computer should present the number of guesses and ask if the player would like to play again.

Let's play Guess the Number.
Pick a difficulty level (1, 2, or 3): 1
I have my number.
What's your guess?
1
Too low. Guess again: 
5
Too high. Guess again:
2
You got it in 3 guesses!
Play again? n
Goodbye!

Extra/Homework
Don't allow any non-numeric data entry.
� During the game,count non-numeric entries as wrong guesses

Extra1:
Map the number of guesses taken to comments:
� 1 guess:"You're a mind reader!"
� 2�4 guesses:"Most impressive."
� 3�6 guesses:"You can do better than that."
� 7 or more guesses:"Better luck next time.

Extra2:
Keep track of previous
guesses and issue an alert that
the user has already guessed that number. Count this as a wrong guess.
 * 
 */

public class Guess {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		Random random = new Random();
		System.out.println("Select the difficulty level(1 , 2 or 3) : ");
		int difficulty = input.nextInt();
		System.out.println("Enter your guess number: ");
		int number = input.nextInt();

		if (difficulty == 1) {
			guessLow(number);
		}
		if (difficulty == 2) {
			guessMed(number);
		}
		if (difficulty == 3) {
			guessHigh(number);
		}

	}

	public static void guessLow(int number) {

		Random random = new Random();
		Scanner input = new Scanner(System.in);

		int guessLowDif = random.nextInt(10);
		System.out.println("Generated low number: " + guessLowDif);

		int contorLow = 0;
		int contorHigh = 0;

		String playAgain = "";

		while (number != guessLowDif) {

			if (number < guessLowDif) {
				System.out.println("Too low. Guess again:");
				number = input.nextInt();
				++contorLow;

			}
			if (number > guessLowDif) {
				System.out.println("Too high. Guess again:");
				number = input.nextInt();
				++contorHigh;
			}

			int contorSum = contorLow + contorHigh;

			if (number == guessLowDif) {

				System.out.println("You got it in " + contorSum + " guesses!");

				System.out.println("Would you like to play again? Enter Y to play or any other key to quit: ");

				do {
					playAgain = input.nextLine();

				} while (playAgain.length() < 1);

				if (playAgain.trim().equalsIgnoreCase("y")) {
					guessLow(number);
				} else {
					System.out.println("Good Bye!!!");
				}

			}
		}
	}

	public static void guessMed(int number) {

		Random random = new Random();
		Scanner input = new Scanner(System.in);
		String playAgain = "";

		int guessMedDif = random.nextInt(100);
		System.out.println("Generated low number: " + guessMedDif);

		int contorLow = 0;
		int contorHigh = 0;

		while (number != guessMedDif) {
			if (number < guessMedDif) {
				System.out.println("Too low. Guess again:");
				number = input.nextInt();
				++contorLow;

			}
			if (number > guessMedDif) {
				System.out.println("Too high. Guess again:");
				number = input.nextInt();
				++contorHigh;
			}

			int contorSum = contorLow + contorHigh;

			if (number == guessMedDif) {

				System.out.println("You got it in " + contorSum + " guesses!");

				System.out.println("Would you like to play again? Enter Y to play or any other key to quit: ");

				do {
					playAgain = input.nextLine();

				} while (playAgain.length() < 1);

				if (playAgain.trim().equalsIgnoreCase("y")) {
					guessLow(number);
				} else {
					System.out.println("Good Bye!!!");
				}
			}
		}
	}

	public static void guessHigh(int number) {

		Random random = new Random();
		Scanner input = new Scanner(System.in);
		String playAgain = "";

		int guessHighDif = random.nextInt(1000);
		System.out.println("Generated high number: " + guessHighDif);

		int contorLow = 0;
		int contorHigh = 0;

		while (number != guessHighDif) {
			if (number < guessHighDif) {
				System.out.println("Too low. Guess again:");
				number = input.nextInt();
				++contorLow;

			}
			if (number > guessHighDif) {
				System.out.println("Too high. Guess again:");
				number = input.nextInt();
				++contorHigh;
			}

			int contorSum = contorLow + contorHigh;

			if (number == guessHighDif) {

				System.out.println("You got it in " + contorSum + " guesses!");

				System.out.println("Would you like to play again? Enter Y to play or any other key to quit: ");

				do {
					playAgain = input.nextLine();

				} while (playAgain.length() < 1);

				if (playAgain.trim().equalsIgnoreCase("y")) {
					guessLow(number);
				} else {
					System.out.println("Good Bye!!!");
				}
			}
		}
	}
}
