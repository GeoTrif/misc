package com.sda.geotrif;

import java.util.Random;
import java.util.Scanner;

public class GuessGeneral {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.println("Guess Game. ");
		System.out.println("The difficulty level is: 10 * 10 * inputed difficulty.");
		System.out.println("Enter the guess number: ");
		int number = input.nextInt();

		guessGeneral(number);

	}

	public static void guessGeneral(int number) {

		Random random = new Random();
		Scanner input = new Scanner(System.in);

		System.out.println("Input the difficulty level: ");
		int level = input.nextInt();

		int guessDif = random.nextInt(10 * 10 * level);
		System.out.println("Generated low number: " + guessDif);

		int contorLow = 0;
		int contorHigh = 0;

		String playAgain = "";

		while (number != guessDif) {

			if (number < guessDif) {
				System.out.println("Too low. Guess again:");
				number = input.nextInt();
				++contorLow;

			}
			if (number > guessDif) {
				System.out.println("Too high. Guess again:");
				number = input.nextInt();
				++contorHigh;
			}

			int contorSum = contorLow + contorHigh;

			if (number == guessDif) {

				System.out.println("You got it in " + contorSum + " guesses!");

				if (contorSum >= 0 && contorSum <= 1) {
					System.out.println("You're a mind reader!");
				} else if (contorSum >= 2 && contorSum <= 4) {
					System.out.println("Most impressive.");
				} else if (contorSum >= 3 && contorSum <= 6) {
					System.out.println("You can do better than that.");
				} else if (contorSum >= 7) {
					System.out.println("Better luck next time.");
				}

				System.out.println("Would you like to play again? Enter Y to play or any other key to quit: ");

				do {
					playAgain = input.nextLine();

				} while (playAgain.length() < 1);

				if (playAgain.trim().equalsIgnoreCase("y")) {
					guessGeneral(number);
				} else {
					System.out.println("Good Bye!!!");
				}

			}
		}

	}

}
