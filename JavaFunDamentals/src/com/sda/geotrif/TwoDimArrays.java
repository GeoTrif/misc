package com.sda.geotrif;

import java.util.Arrays;
import java.util.Scanner;

/*
 * Write a program which takes 2 digits, X,Y as input and generates a 2-dimensional array. The element value in the i-th row and j-th column of the array should be i*j.
Example
Suppose the following inputs are given to the program:
3,5
Then, the output of the program should be:
[0, 0, 0, 0, 0]
[0, 1, 2, 3, 4]
[0, 2, 4, 6, 8]
 * 
 * 
 */

public class TwoDimArrays {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Enter the number of rows: ");
		int rows = input.nextInt();

		System.out.println("Enter the number of columns: ");
		int col = input.nextInt();

		printTowDimArray(rows, col);

		System.out.println("----------");

		int[][] myArray = printArray(rows, col);

		System.out.println("The elements of the 2DimArray are: " + Arrays.deepToString(myArray));

		printArraySecondary(rows, col);

	}

	public static void printTowDimArray(int rows, int col) {

		for (int i = 0; i <= rows - 1; i++) {
			for (int j = 0; j <= col - 1; j++) {
				System.out.print(i * j + " ");
			}
			System.out.println();
		}
	}

	public static int[][] printArray(int rows, int col) {

		int[][] array = new int[rows][col];

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length; j++) {
				if (i != j) {
					System.out.print(0 + " ");
					array[i][j] = 0;
				} else if (i == j) {
					System.out.print(i * j + " ");
					array[i][j] = i * j;
				}
			}

			System.out.println();

		}

		return array;

	}

	public static int[][] printArraySecondary(int rows, int col) {

		int[][] array = new int[rows][col];
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length; j++) {
				if (i + j == rows - 1) {
					System.out.print(i * j + " ");
					array[i][j] = i * j;
				} else {
					System.out.print(0 + " ");
					array[i][j] = 0;
				}
			}

			System.out.println();

		}

		return array;

	}

}
