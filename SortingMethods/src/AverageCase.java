
/**
 * Metoda BubbleSort este cel mai simplu algoritm de sortare care functioneaza interschimband in mod repetat elementele adiacente daca nu sunt in ordine crescatoare. 
 * Time complexity in cazul cel mai defavorabil este atunci cand vectorul este in ordine descrescatoare.Cazul mediu este atunci cand vectorul are elementele in ordine aleatoare,dar nu descrescatoare si crescatoare.
 * Cazul cel mai favorabil este atunci cand vectorul nostru este deja sortat.
 * Complexitatea in cazul cel mai defavorabil si in cazul mediu este de O(n * n).In cazul cel mai favorabil este O(n).
 * Acest algoritm este folosit doar pentru introducerea in conceptul algoritmilor de sortare.
 * 
 * 
 * Metoda Counting Sort este un algoritm de sortare bazat pe niste chei dintr-un interval specificat.Functioneaza numarand numarul de obiecte care au valori ale cheilor distincte,dupa care se calculeaza pozitia fiecarui obiect
 * in secventa care va afisa vectorul sortat.
 * Complexitatea timpului de rulare a algoritmului este in toate cele 3 cazuri(cel mai defavorabil,mediocru si cel mai favorabil) O(n + k).
 * Acest algoritm este eficient daca numarul datelor de intrare nu este cu mult mai mare decat numarul de obiecte care este sortat.Nu este un algoritm bazat pe comparatii.
 * De obicei este folosit ca si o subrutina pentru algoritmul Radix Sort.
 * 
 * 
 * Metoda Insertion Sort este un algoritm simplu de sortare care functioneaza la fel cum sortam niste carti de joc in mana.
 * Time complexity in cazul cel mai defavorabil este atunci cand vectorul este in ordine descrescatoare.Cazul mediu este atunci cand vectorul are elementele in ordine aleatoare,dar nu descrescatoare si crescatoare.
 * Cazul cel mai favorabil este atunci cand vectorul nostru este deja sortat.
 * Complexitatea in cazul cel mai defavorabil si in cazul mediu este de O(n * n).In cazul cel mai favorabil este O(n).
 * Paradigma algoritmului este abordarea incrementarii.
 * Acest algoritm este folosit atunci cand avem un vector cu putine elemente.
 * 
 * 
 *  Algoritmul QuickSort este un algoritm cu paradigmul "Divide annd Conquer".Acest algoritm alege un element ca si pivot si partitioneaza 
 * vectorul dat in jurul pivotului ales.Sunt mai multe versiuni ale acestui algoritm care alege pivotul in mod diferit.
 * De exemplu: 
 * 1.Alege pivotul ca fiind primul element din vector.
 * 2.Alege pivotul ca fiind ultimul element din vector(implementata in programul de mai jos).
 * 3.Alege pivotul ca fiind un element arbitrar din vector.
 * 4.Alege pivotul ca fiind elementul median din vector.
 * Procesul cheie al acestui algoritm este reprezentat de metoda partition().Scopul acestei metode este acela de a pune un element x ales ca si pivot din vectorul dat in pozitia lui corecta in vectorul sortat si sa puna toate
 * elementele mai mici decat pivotul inaintea pivotului,iar cele mai mari decat pivotul dupa el.
 * Cazul cel mai defavorabil al timpului de rulare este atunci cand metoda partition() intotdeauna alege ultimul sau primul element ca si pivot.
 * Cazul cel mai favorabil este atunci cand metoda partition() alege intotdeauna elementul median al vectorului.
 * Cazul mediocru este atunci cand elementul pivot nu este primul,ultimul sau elementul median.

 */
import java.io.File;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import org.omg.Messaging.SyncScopeHelper;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class AverageCase {

	public static int swapContorBubbleSort;
	public static int assigmentBubbleSort;
	public static int operationsSumBubbleSort;

	public static int swapContorCountingSort;
	public static int assigmentCountingSort;
	public static int operationsSumCountingSort;

	public static int swapContorInsertionSort;
	public static int assigmentInsertionSort;
	public static int operationsSumInsertionSort;

	public static int swapContorQuickSort;
	public static int assigmentQuickSort;
	public static int operationsSumQuickSort;

	public static void main(String[] args) throws Exception {

		File f = new File("C:\\Users\\GeoTrif\\Desktop\\excel.xlss");

		Scanner input = new Scanner(System.in);
		Random random = new Random();

		WritableWorkbook myExcel = Workbook.createWorkbook(f);
		WritableSheet mySheet = myExcel.createSheet("mySheet", 0);

		System.out.println("Input the number of elements of the array: ");
		int elementNum = input.nextInt();

		int[] myArrayBubbleSort = new int[elementNum];
		char[] myArrayCountingSort = new char[elementNum];
		int[] myArrayInsertionSort = new int[elementNum];
		int[] myArrayQuickSort = new int[elementNum];

		for (int i = 0; i < myArrayBubbleSort.length; i++) {

			myArrayBubbleSort[i] = random.nextInt(10000);
			assigmentBubbleSort++;

		}

		for (int i = 0; i < myArrayCountingSort.length; i++) {

			myArrayCountingSort[i] = (char) (97 + random.nextInt(25));

			assigmentCountingSort++;
		}
		for (int i = 0; i < myArrayInsertionSort.length; i++) {

			myArrayInsertionSort[i] = random.nextInt(10000);
			assigmentInsertionSort++;
		}

		for (int i = 0; i < myArrayQuickSort.length; i++) {

			myArrayQuickSort[i] = random.nextInt(10000);
			assigmentQuickSort++;
		}

		System.out.println("The original array for Bubble Sort is: " + Arrays.toString(myArrayBubbleSort));
		System.out.println("The original array for Insertion Sort is: " + Arrays.toString(myArrayInsertionSort));
		System.out.println("The original array for Quick Sort is: " + Arrays.toString(myArrayQuickSort));

		System.out.println("-------------------------------------");

		int[] sortedArrayBubbleSort = bubbleSort(myArrayBubbleSort);
		char[] sortedArrayCountingSort = countingSort(myArrayCountingSort);
		int[] sortedArrayInsertionSort = insertionSort(myArrayInsertionSort);
		quickSort(myArrayQuickSort, 0, myArrayQuickSort.length - 1);

		operationsSumBubbleSort = swapContorBubbleSort + assigmentBubbleSort;
		operationsSumCountingSort = swapContorCountingSort + assigmentCountingSort;
		operationsSumInsertionSort = swapContorInsertionSort + assigmentInsertionSort;
		operationsSumQuickSort = swapContorQuickSort + assigmentQuickSort;

		System.out.println("The sorted array for Bubble Sort is: " + Arrays.toString(sortedArrayBubbleSort));
		System.out.println("The sorted array for Counting Sort is: " + Arrays.toString(sortedArrayCountingSort));
		System.out.println("The sorted array for Insertion Sort is: " + Arrays.toString(sortedArrayInsertionSort));
		System.out.println("The sorted array for Quick Sort is: " + Arrays.toString(myArrayQuickSort));

		Label start = new Label(0, 0, "Sorting algorithms comparison");
		Label l1BubbleSort = new Label(0, 1, "BubbleSort");
		Label l2BubbleSort = new Label(0, 2, "Number of swaps");
		Label lSwapBubbleSort = new Label(1, 2, Integer.toString(swapContorBubbleSort));
		Label l3BubbleSort = new Label(0, 3, "Number of assigments ");
		Label lAssigmentBubbleSort = new Label(1, 3, Integer.toString(assigmentBubbleSort));
		Label l4BubbleSort = new Label(0, 4, "Total number of operations ");
		Label lTotalBubbleSort = new Label(1, 4, Integer.toString(operationsSumBubbleSort));

		Label l1CountingSort = new Label(0, 6, "CountingSort");
		Label l2CountingSort = new Label(0, 7, "Number of swaps");
		Label lSwapCountingSort = new Label(1, 7, Integer.toString(swapContorCountingSort));
		Label l3CountingSort = new Label(0, 8, "Number of assigments ");
		Label lAssigmentCountingSort = new Label(1, 8, Integer.toString(assigmentCountingSort));
		Label l4CountingSort = new Label(0, 9, "Total number of operations ");
		Label lTotalCountingSort = new Label(1, 9, Integer.toString(operationsSumCountingSort));

		Label l1InsertionSort = new Label(0, 11, "InsertionSort");
		Label l2InsertionSort = new Label(0, 12, "Number of swaps");
		Label lSwapInsertionSort = new Label(1, 12, Integer.toString(swapContorInsertionSort));
		Label l3InsertionSort = new Label(0, 13, "Number of assigments ");
		Label lAssigmentInsertionSort = new Label(1, 13, Integer.toString(assigmentInsertionSort));
		Label l4InsertionSort = new Label(0, 14, "Total number of operations ");
		Label lTotalInsertionSort = new Label(1, 14, Integer.toString(operationsSumInsertionSort));

		Label l1QuickSort = new Label(0, 16, "QuickSort");
		Label l2QuickSort = new Label(0, 17, "Number of swaps");
		Label lSwapQuickSort = new Label(1, 17, Integer.toString(swapContorQuickSort));
		Label l3QuickSort = new Label(0, 18, "Number of assigments ");
		Label lAssigmentQuickSort = new Label(1, 18, Integer.toString(assigmentQuickSort));
		Label l4QuickSort = new Label(0, 19, "Total number of operations ");
		Label lTotalQuickSort = new Label(1, 19, Integer.toString(operationsSumQuickSort));

		mySheet.addCell(start);
		mySheet.addCell(l1BubbleSort);
		mySheet.addCell(l2BubbleSort);
		mySheet.addCell(lSwapBubbleSort);
		mySheet.addCell(l3BubbleSort);
		mySheet.addCell(lAssigmentBubbleSort);
		mySheet.addCell(l4BubbleSort);
		mySheet.addCell(lTotalBubbleSort);

		mySheet.addCell(l1CountingSort);
		mySheet.addCell(l2CountingSort);
		mySheet.addCell(lSwapCountingSort);
		mySheet.addCell(l3CountingSort);
		mySheet.addCell(lAssigmentCountingSort);
		mySheet.addCell(l4CountingSort);
		mySheet.addCell(lTotalCountingSort);

		mySheet.addCell(l1InsertionSort);
		mySheet.addCell(l2InsertionSort);
		mySheet.addCell(lSwapInsertionSort);
		mySheet.addCell(l3InsertionSort);
		mySheet.addCell(lAssigmentInsertionSort);
		mySheet.addCell(l4InsertionSort);
		mySheet.addCell(lTotalInsertionSort);

		mySheet.addCell(l1QuickSort);
		mySheet.addCell(l2QuickSort);
		mySheet.addCell(lSwapQuickSort);
		mySheet.addCell(l3QuickSort);
		mySheet.addCell(lAssigmentQuickSort);
		mySheet.addCell(l4QuickSort);
		mySheet.addCell(lTotalQuickSort);

		myExcel.write();
		myExcel.close();

		System.out.println("Excel created.");

	}

	public static int[] bubbleSort(int[] array) { // Metoda care implementeaza Bubble Sort.

		int arrayLen = array.length;
		int temp = array[0];

		for (int i = 0; i < arrayLen - 1; i++) { // Bubble Sort utilizeaza metoda BruteForce cu 2 for-uri.
			for (int j = 0; j < arrayLen - i - 1; j++) {
				if (array[j] > array[j + 1]) {

					temp = array[j]; // Facem interschimbare de variabile utilizand o a treia variabila temp.
					array[j] = array[j + 1];
					array[j + 1] = temp;
					swapContorBubbleSort++;

				}
			}
		}

		return array;

	}

	public static char[] countingSort(char[] array) {

		int length = array.length;

		char[] output = new char[length]; // Vectorul de caractere rezultat care va avea vectorul sortat array.

		int[] count = new int[256]; // Am creat un vector count sa stocam toate caracterele individuale.

		for (int i = 0; i < 256; ++i) {
			count[i] = 0; // Initializam vectorul count cu 0.
		}

		for (int i = 0; i < length; ++i) { // Stocam count pentru fiecare caracter din array.

			++count[array[i]];

		}

		for (int i = 1; i <= 255; ++i) {

			count[i] += count[i - 1]; // Schimbam count[i] astfel incat count[i] sa contina pozitia actuala a
			swapContorCountingSort++; // caracterului din vectorul rezultat.

		}

		for (int i = 0; i < length; ++i) {

			output[count[array[i]] - 1] = array[i]; // Construim vectorul de caractere rezultat.
			--count[array[i]];
		}

		for (int i = 0; i < length; ++i) { // Copiem vectorul rezultat vectorului nostru array astfel incat array sa
											// contina caracterele sortate.
			array[i] = output[i];
		}

		return array;

	}

	public static int[] insertionSort(int[] array) { // Metoda pentru a sorta vectorul cu algoritmul insertion sort.

		int arrayLen = array.length;

		for (int i = 1; i < arrayLen; ++i) {

			int key = array[i];

			int j = i - 1;

			while (j >= 0 && array[j] > key) { // Mutarea elementelor din array[0..i-1],care sunt mai mari decat key,cu
												// o pozitie mai in fata decat pozitia curenta.

				array[j + 1] = array[j];
				j = j - 1;
				swapContorInsertionSort++;
			}

			array[j + 1] = key;

		}

		return array;
	}

	// QuickSort.
	public static int quickPartition(int[] array, int low, int high) {

		int pivot = array[high];
		int i = low - 1; // Indexul elementelor mai mici decat pivotul.
		int temp = 0;

		for (int j = low; j < high; j++) {

			if (array[j] <= pivot) { // Daca elementul curent este mai mic sau egal cu pivotul.

				i++;

				temp = array[i]; // Interschimba array[i] cu array[j];
				array[i] = array[j];
				array[j] = temp;
				swapContorQuickSort++;

			}

		}

		temp = array[i + 1]; // Interschimba array[i + 1] cu array[high] (sau array[pivot]).
		array[i + 1] = array[high];
		array[high] = temp;

		return i + 1;
	}

	/*
	 * Metoda care implementeaza algoritmul QuickSort. Array[] --> vectorul ce
	 * trebuie sortat. low --> indexul de start. high --> indexul de sfarsit.
	 */

	public static void quickSort(int[] array, int low, int high) {

		if (low < high) {

			int pi = quickPartition(array, low, high); // pi este indexul de partitie,array[pi] este acuma la indexul
														// potrivit.

			quickSort(array, low, pi - 1); // Sortare recursiva a elementelor inaintea partitionarii.
			quickSort(array, pi + 1, high); // Sortare recursiva a elementelor dupa partitionare.

		}

	}

}
