/**
 * Algoritmul QuickSort este un algoritm cu paradigmul "Divide annd Conquer".Acest algoritm alege un element ca si pivot si partitioneaza 
 * vectorul dat in jurul pivotului ales.Sunt mai multe versiuni ale acestui algoritm care alege pivotul in mod diferit.
 * De exemplu: 
 * 1.Alege pivotul ca fiind primul element din vector.
 * 2.Alege pivotul ca fiind ultimul element din vector(implementata in programul de mai jos).
 * 3.Alege pivotul ca fiind un element arbitrar din vector.
 * 4.Alege pivotul ca fiind elementul median din vector.
 * Procesul cheie al acestui algoritm este reprezentat de metoda partition().Scopul acestei metode este acela de a pune un element x ales ca si pivot din vectorul dat in pozitia lui corecta in vectorul sortat si sa puna toate
 * elementele mai mici decat pivotul inaintea pivotului,iar cele mai mari decat pivotul dupa el.
 * Cazul cel mai defavorabil al timpului de rulare este atunci cand metoda partition() intotdeauna alege ultimul sau primul element ca si pivot.
 * Cazul cel mai favorabil este atunci cand metoda partition() alege intotdeauna elementul median al vectorului.
 * Cazul mediocru este atunci cand elementul pivot nu este primul,ultimul sau elementul median.
 * 
 */

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class QuickSort {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		Random random = new Random();

		System.out.println("Enter the number of elements of the array: ");
		int elements = input.nextInt();

		int[] myArray = new int[elements];

		for (int i = 0; i < myArray.length; i++) {

			System.out.println("Enter the value found at index " + i + " :");
			myArray[i] = input.nextInt();

		}

		System.out.println("The original array is: " + Arrays.toString(myArray));

		int arrayLen = myArray.length;

		sort(myArray, 0, arrayLen - 1);

		System.out.println("The sorte array is : " + Arrays.toString(myArray));

	}

	/*
	 * Aceasta functie ia ultimul element ca si pivot,pozitioneaza elementul pivot
	 * la pozitia lui corecta in vectorul sortat, si pune toate elementele mai mici
	 * decat pivotul in stanga pivotului si elementele mai mari decat pivotul in
	 * dreapta sa.
	 * 
	 */

	public static int partition(int[] array, int low, int high) {

		int pivot = array[high];
		int i = low - 1; // Indexul elementelor mai mici decat pivotul.
		int temp = 0;

		for (int j = low; j < high; j++) {

			if (array[j] <= pivot) { // Daca elementul curent este mai mic sau egal cu pivotul.

				i++;

				temp = array[i]; // Interschimba array[i] cu array[j];
				array[i] = array[j];
				array[j] = temp;

			}

		}

		temp = array[i + 1]; // Interschimba array[i + 1] cu array[high] (sau array[pivot]).
		array[i + 1] = array[high];
		array[high] = temp;

		return i + 1;
	}

	/*
	 * Metoda care implementeaza algoritmul QuickSort. Array[] --> vectorul ce
	 * trebuie sortat. low --> indexul de start. high --> indexul de sfarsit.
	 */

	public static void sort(int[] array, int low, int high) {

		if (low < high) {

			int pi = partition(array, low, high); // pi este indexul de partitie,array[pi] este acuma la indexul
													// potrivit.

			sort(array, low, pi - 1); // Sortare recursiva a elementelor inaintea partitionarii.
			sort(array, pi + 1, high); // Sortare recursiva a elementelor dupa partitionare.
		}

	}

}
