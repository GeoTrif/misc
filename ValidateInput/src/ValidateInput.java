import java.util.Scanner;

public class ValidateInput {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int number = 0;
		boolean isNumber;
		int wrongContor = 0;

		System.out.println("Enter a whole number please: ");

		do {
			if (input.hasNextInt()) {
				number = input.nextInt();
				isNumber = true;
			} else {
				isNumber = false;
				++wrongContor;
				System.out.println("Incorrect input.You have " + wrongContor + " wrong answers.");
				input.next();

			}

		} while (!(isNumber));
		System.out.println(number);

	}

}
